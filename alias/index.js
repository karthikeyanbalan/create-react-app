const path = require("path");
const getRootPath = (srcPath) => path.resolve(__dirname, srcPath);

module.exports = {
  "@component": getRootPath("../src/components"),
  "@fixture": getRootPath("../src/fixture"),
};
