const routes = [
  {
    path: "/docs/:dynamicId",
    exact: true,
    component: "Dynamic",
  },
  {
    path: ["/", "/home"],
    exact: true,
    component: "Home",
  },
  {
    path: "/docs",
    exact: true,
    component: "Docs",
  },
]

export default routes
