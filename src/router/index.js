import { lazy, Suspense } from "react"
import { Switch, Route } from "react-router-dom"
import * as ComponentKit from "@component"

import routes from "./config"
import GlobalStyles from "../globalStyles"

const Router = () => {
  console.log("ComponentKit", ComponentKit)
  return (
    <Suspense fallback={null}>
      <GlobalStyles />
      <ComponentKit.Header />
      <Switch>
        {routes.map((routeItem) => {
          return (
            <Route
              key={routeItem.component}
              path={routeItem.path}
              exact={routeItem.exact}
              component={lazy(() => import(`../pages/${routeItem.component}`))}
            />
          )
        })}
      </Switch>
      <ComponentKit.Footer />
    </Suspense>
  )
}

export default Router
