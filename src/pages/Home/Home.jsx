import { lazy } from "react"
import * as Fixture from "@fixture"
import * as ComponentKit from "@component"
const {
  IntroContent,
  MiddleBlockContent,
  AboutContent,
  MissionContent,
  ProductContent,
  ContactContent,
} = Fixture

const Container = lazy(() => import("../../common/Container"))
const ScrollToTop = lazy(() => import("../../common/ScrollToTop"))

const Home = () => {
  return (
    <Container>
      <ScrollToTop />
      <ComponentKit.ContentBlock
        type="right"
        first="true"
        title={IntroContent.title}
        content={IntroContent.text}
        button={IntroContent.button}
        icon="developer.svg"
        id="intro"
      />
      <ComponentKit.MiddleBlock
        title={MiddleBlockContent.title}
        content={MiddleBlockContent.text}
        button={MiddleBlockContent.button}
      />
      <ComponentKit.ContentBlock
        type="left"
        title={AboutContent.title}
        content={AboutContent.text}
        section={AboutContent.section}
        icon="graphs.svg"
        id="about"
      />
      <ComponentKit.ContentBlock
        type="right"
        title={MissionContent.title}
        content={MissionContent.text}
        icon="product-launch.svg"
        id="mission"
      />

      <ComponentKit.ContentBlock
        type="left"
        title={ProductContent.title}
        content={ProductContent.text}
        icon="waving.svg"
        id="product"
      />
      {/* <ComponentKit.ContactForm
        title={ContactContent.title}
        content={ContactContent.text}
        id="contact"
      /> */}
    </Container>
  )
}

export { Home }
export default Home
