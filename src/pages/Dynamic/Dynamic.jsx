import React, { lazy } from "react"
import styled from "styled-components"
import * as ComponentKit from "@component"
import * as Fixture from "@fixture"
const { DocsIntroContent, DocList } = Fixture
const Container = styled.div`
  min-height: 80rem;
  position: relative;
  width: 100%;
  overflow: hidden;
  border-top: ${(props) => (props.border ? "1px solid #CDD1D4" : "")};
  .rightcontentblock {
    padding: 4rem 0;
  }
  @media only screen and (max-width: 1024px) {
    max-width: 950px;
  }

  @media only screen and (max-width: 768px) {
    max-width: 700px;
  }

  @media only screen and (max-width: 414px) {
    max-width: 370px;
  }
`
const InnerHtmlContainer = styled.div`
  position: relative;
  width: 100%;
  max-width: 1280px;
  margin: 4rem auto;
  margin-top: 10px;
  .image-content {
    padding: 4rem;
    display: flex;
    justify-content: center;
    background: #e6e6e6;
    margin-bottom: 2rem;
  }
  strong {
    font-weight: bold;
  }

  h5 {
    font-size: 1.5rem;
  }
  iframe.youtube {
    margin: auto;
    width: 90%;
    height: 32vw;
    display: flex;
  }

  button {
  }
  button.btn-learn {
    width: 50%;
    height: 56px;
    margin: auto;
    display: block;
    text-align: center;
    margin-top: 2rem;
    &.btn-primary {
      color: #fff;
      background-color: #f89234;
      border-color: #f99937;
      font-size: 1.5rem;
    }
  }
  .button-grp-kit {
    display: flex;
  }
  .button-a-kit {
    width: 25%;
    margin-right: 1rem;
  }
  button.btn-learn-02 {
    width: 100%;
    min-height: 200px;
    display: block;
    text-align: center;
    margin-right: 1rem;
    &.btn-primary {
      color: #fff;
      background-color: #f89234;
      border-color: #f99937;
      font-size: 1.5rem;
    }
  }
  .video-heading {
    text-align: center;
  }
`

const Dynamic = (props) => {
  const { match } = props || {}
  const {
    params: { dynamicId },
  } = match

  const getHtml = () => DocList.find((item) => item.pageName === dynamicId)
  const { innerHtml } = getHtml() || {}
  console.log(innerHtml)

  const BreadcrumbList = [
    {
      name: "MATERIAL",
      url: "/",
    },
    {
      name: "DOCS",
      url: "/docs",
    },
    {
      name: dynamicId.toUpperCase(),
      url: "",
      disable: true,
    },
  ]

  return (
    <Container>
      <ComponentKit.Breadcrumb list={BreadcrumbList} />
      <InnerHtmlContainer dangerouslySetInnerHTML={{ __html: innerHtml }} />
    </Container>
  )
}

export { Dynamic }
export default Dynamic
