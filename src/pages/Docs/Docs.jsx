import { lazy } from "react"
import * as Fixture from "@fixture"
import * as ComponentKit from "@component"
import styled from "styled-components"
import { CardList } from "./CardList"
const {
  IntroContent,
  MiddleBlockContent,
  AboutContent,
  MissionContent,
  ProductContent,
  ContactContent,
  DocsIntroContent,
  DocList,
} = Fixture

const Container = styled.div`
  min-height: 80rem;
  position: relative;
  width: 100%;
  overflow: hidden;
  border-top: ${(props) => (props.border ? "1px solid #CDD1D4" : "")};
  .rightcontentblock {
    padding: 4rem 0;
  }
  @media only screen and (max-width: 1024px) {
    max-width: 950px;
  }

  @media only screen and (max-width: 768px) {
    max-width: 700px;
  }

  @media only screen and (max-width: 414px) {
    max-width: 370px;
  }
`
const BannerContainer = styled.div`
  position: relative;
  width: 100%;
  background: #f1f1f1;

  .banner-kit {
    max-width: 1280px;
    margin: auto;
    padding: 6rem 15px;
  }
`

const ScrollToTop = lazy(() => import("../../common/ScrollToTop"))

const Docs = () => {
  console.log(DocList)
  const BreadcrumbList = [
    {
      name: "MATERIAL",
      url: "/",
    },
    {
      name: "DOCS",
      disable: true,
    },
  ]
  return (
    <Container>
      <ScrollToTop />
      <BannerContainer>
        <ComponentKit.ContentBlock
          className="banner-kit"
          type="right"
          first="true"
          title={DocsIntroContent.title}
          content={DocsIntroContent.text}
          button={DocsIntroContent.button}
          id="intro"
        />
      </BannerContainer>
      <ComponentKit.Breadcrumb list={BreadcrumbList} />
      <CardList />
    </Container>
  )
}

export { Docs }
export default Docs
