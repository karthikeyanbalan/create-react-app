import React from "react"
import * as Fixture from "@fixture"
import styled from "styled-components"
import { Card, Button } from "react-bootstrap"
import { Link } from "react-router-dom"
const { DocList } = Fixture
const CardKit = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 1.25rem;
  width: 75%;
  margin: 4rem auto;
  max-width: 1280px;
  margin-top: 2rem;

  p {
    overflow: hidden;
    height: 113px;
  }
`
const CardUI = styled(Card)`
  width: 100%;
  min-height: 300px;
  border: 1px solid #13a2d1;
`
const Title = styled(Card.Title)`
  font-size: 30px;
  font-weight: bold;
`
const ButtonUI = styled(Button)`
  background-color: #13a1d2;
  border-color: #13a1d2;
  position: absolute;
  bottom: 18px;
`

function CardList(props) {
  return (
    <CardKit>
      {DocList.length > 0 &&
        DocList.map((item) => {
          const { name, pageName, description = "" } = item
          const defaultDesc =
            description ||
            `Some quick example text to build on the card title and make
                    up the bulk of the card's content.`
          return (
            <Link to={`/docs/${pageName}`}>
              <CardUI>
                <Card.Body>
                  <Title>{name}</Title>
                  <Card.Text>{defaultDesc}</Card.Text>
                  <ButtonUI variant="primary">Learn More</ButtonUI>
                </Card.Body>
              </CardUI>
            </Link>
          )
        })}
    </CardKit>
  )
}

export { CardList }
export default CardList
