export const htmlData = `
<div class="image-content"><img src="//upload.wikimedia.org/wikipedia/commons/thumb/6/61/HTML5_logo_and_wordmark.svg/240px-HTML5_logo_and_wordmark.svg.png" /></div>
<h1 class="display-4">HTML</h1>
<p class="lead">
  HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content. Other technologies besides HTML are generally used to describe a web page's appearance/presentation (CSS) or functionality/behavior (JavaScript).
</p>
<p class="lead">
  HTML Introduction, If you're new to Web development, be sure to read our HTML Basics article to learn what HTML is and how to use it.
</p>
<br />
<p class="lead">
  HTML uses "markup" to annotate text, images, and other content for display in a Web browser. HTML markup includes special "elements" such as <strong>head</strong>, <strong>title</strong>, <strong>body</strong>, <strong>header</strong>, <strong>footer</strong>, <strong>article</strong>, <strong>section</strong>, <strong>p</strong>, <strong>div</strong>, <strong>span</strong>, <strong>img</strong>, <strong>aside</strong>, <strong>audio</strong>, <strong>canvas</strong>, <strong>datalist</strong>, <strong>details</strong>, <strong>embed</strong>, <strong>nav</strong>, <strong>output</strong>, <strong>progress</strong>, <strong>video</strong>, <strong>ul</strong>, <strong>ol</strong>, <strong>li</strong> and many others..
</p>
<br />
<br />
<br />
<h5>Please study UI like to split like below mentioned:</h5>
<ul class="list-group">
  <li class="list-group-item"><strong>Template </strong> -  [div, span, table, Tr, td,, Article, aside,… etc]</li>
  <li class="list-group-item"><strong>Typography</strong> -  [p, label, h1...h9,...etc]</li>
  <li class="list-group-item"><strong>Form</strong> - [input, button checkbox, radio, form, label, select]</li>
  <li class="list-group-item"><strong>Media</strong> - [ img, video, audio, iframe ..etc]</li>
  <li class="list-group-item"><strong>Advanced</strong> - [Canvas, Svg, ...etc]</li>
</ul>
<br />
<br />
<hr />
<br />
<br />
<h1 class="video-heading">HTML Tutorial</h1>
<iframe class="youtube" src="https://www.youtube.com/embed/qz0aGYrrlhU">
</iframe>
<br />
<br />
<hr />
<br />
<br />
<h3>HTML Global Attributes</h3>
<p>lobal attributes are attributes common to all HTML elements; they can be used on all elements, though they may have no effect on some elements.
Global attributes may be specified on all HTML elements, even those not specified in the standard. That means that any non-standard elements must still permit these attributes, even though using those elements means that the document is no longer HTML5-compliant. For example, HTML5-compliant browsers hide content marked as <foo hidden>...</foo>, even though <foo> is not a valid HTML element.</p>
<br />
<br />
<a href="//developer.mozilla.org/en-US/docs/Web/HTML" target="_blank"><button type="button" class="btn-learn btn btn-primary">LEARN MORE HTML</button></a>
<br />
<br />
<hr />
<br />
<br />
<h1  class="video-heading">HTML ARIA ADA </h1>
<iframe class="youtube" src="https://www.youtube.com/embed/g9Qff0b-lHk">
</iframe>
<br />
<br />
<hr />
<br />
<br />
<h3>HTML Snippets Tricks</h3>
<p>Well organized and easy to understand Web create tutorials with lots of examples of how to use HTML</p>
<br />
<div class="button-grp-kit">
<a class="button-a-kit" href="//css-tricks.com/snippets/html/" target="_blank"><button type="button" class="btn-learn-02 btn btn-primary">Learn More Html Snippets Tricks</button></a>
<a class="button-a-kit" href="//www.w3schools.com/html/default.asp" target="_blank"><button type="button" class="btn-learn-02 btn btn-primary">w3schools</button></a>
<a class="button-a-kit" href="//dev.to/effingkay/front-end-checklist-3cc2" target="_blank"><button type="button" class="btn-learn-02 btn btn-primary">front end checklist</button></a>
<a class="button-a-kit" href="//www.w3.org/TR/WCAG21/" target="_blank"><button type="button" class="btn-learn-02 btn btn-primary">WCAG21</button></a>
</div>
<br />
<br />
<hr />
<br />
<br />

`
export const cssData = `
<div class="image-content"><img src="//upload.wikimedia.org/wikipedia/commons/thumb/d/d5/CSS3_logo_and_wordmark.svg/240px-CSS3_logo_and_wordmark.svg.png" /></div>
<h1 class="display-4">CSS</h1>
<p class="lead">
  Cascading Style Sheets (CSS) is a style sheet language used for describing the presentation of a document written in a markup language such as HTML. CSS is a cornerstone technology of the World Wide Web, alongside HTML and JavaScript.
  <br/>
  <br/>
  CSS is among the core languages of the open web and is standardized across Web browsers according to W3C specifications. Previously, development of various parts of CSS specification was done synchronously, which allowed versioning of the latest recommendations. You might have heard about CSS1, CSS2.1, CSS3. However, CSS4 has never become an official version.
</p>

  <ul>
  <li><a href="https://developer.mozilla.org/en-US/docs/Web/CSS/Reference">CSS reference</a>: This exhaustive reference for seasoned Web developers describes every property and concept of CSS.</li>
  <li>CSS key concepts:
    <ul>
    <li>The <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/Syntax">syntax and forms of the language</a></li>
    <li><a href="https://developer.mozilla.org/en-US/docs/Web/CSS/Specificity">Specificity</a>, <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/inheritance">inheritance</a>, and <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/Cascade">the&nbsp;Cascade</a></li>
    <li><a href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Values_and_Units">CSS units and values</a> and <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Functions">functional notations</a></li>
    <li><a href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Box_Model/Introduction_to_the_CSS_box_model">Box model</a> and <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Box_Model/Mastering_margin_collapsing">margin collapse</a></li>
    <li>The <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/Containing_block">containing block</a></li>
    <li><a href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Positioning/Understanding_z_index/The_stacking_context" title="The stacking context">Stacking</a> and <a href="https://developer.mozilla.org/en-US/docs/Web/Guide/CSS/Block_formatting_context" title="block formatting context">block-formatting</a> contexts</li>
    <li><a href="https://developer.mozilla.org/en-US/docs/Web/CSS/initial_value">Initial</a>, <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/computed_value">computed</a>, <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/used_value">used</a>, and <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/actual_value">actual</a> values</li>
    <li><a href="https://developer.mozilla.org/en-US/docs/Web/CSS/Shorthand_properties">CSS shorthand properties</a></li>
    <li><a href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout">CSS Flexible Box Layout</a></li>
    <li><a href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout">CSS Grid Layout</a></li>
    <li><a href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors">CSS selectors</a></li>
    <li><a href="https://developer.mozilla.org/en-US/docs/Web/CSS/Media_Queries">Media queries</a></li>
    <li><a href="https://developer.mozilla.org/en-US/docs/Web/CSS/animation">Animation</a></li>
    </ul>
  </li>
  </ul>

<br/>
<br/>
<h3>CSS layout</h3>
<p>At this point we've already looked at CSS fundamentals, how to style text, and how to style and manipulate the boxes that your content sits inside. Now it's time to look at how to place your boxes in the right place in relation to the viewport, and to each other. We have covered the necessary prerequisites so we can now dive deep into CSS layout, looking at different display settings, modern layout tools like flexbox, CSS grid, and positioning, and some of the legacy techniques you might still want to know about.</p>
<br />

`
export const jsData = `
<h1 class="display-4">JS</h1>
`
export const nodeJsData = `
<h1 class="display-4">Node JS</h1>
`
export const reactData = `
<h1 class="display-4">React</h1>
`
export const prettierData = `
<h1 class="display-4">Prettier</h1>
`
export const eslintData = `
<h1 class="display-4">ESLINT</h1>
`
