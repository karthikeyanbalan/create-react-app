import * as Content from "./htmlContent"
export const DocList = [
  {
    name: "HTML, HTML 5",
    pageName: "html",
    description:
      "The HyperText Markup Language, or HTML(HyperText Markup Language) is the standard markup language for documents designed to be displayed in a web browser. It can be assisted by technologies such as Cascading Style Sheets (CSS) and scripting languages such as JavaScript.",
    innerHtml: Content.htmlData,
  },
  {
    name: "CSS3",
    pageName: "css",
    description:
      "CSS is the language we use to style an HTML document. CSS describes how HTML elements should be displayed. ",
    innerHtml: Content.cssData,
  },
  {
    name: "JS",
    pageName: "js",
    description: "",
    innerHtml: Content.jsData,
  },
  {
    name: "NODE JS",
    pageName: "nodejs",
    description: "",
    innerHtml: Content.nodeJsData,
  },
  {
    name: "REACT",
    pageName: "react",
    description: "",
    innerHtml: Content.reactData,
  },
  {
    name: "PRETTIER",
    pageName: "prettier",
    description: "",
    innerHtml: Content.prettierData,
  },
  {
    name: "ESLINT",
    pageName: "eslint",
    description: "",
    innerHtml: Content.eslintData,
  },
]
