import React from "react"
import { Breadcrumb as ReactBreadcrumb } from "react-bootstrap"
import styled from "styled-components"
// import { NavigateNext as NavigateNextIcon } from "@material-ui/icons"
import { Link } from "react-router-dom"
const BreadcrumbsKit = styled.ul`
  list-style: none;
  display: flex;
  height: 40px;
  align-items: center;
  max-width: 1280px;
  margin: auto;
  padding: 0;
  margin-top: 1rem;
`
const BreadcrumbsSeperater = styled.span`
  padding: 0 10px;
`

const BreadcrumWrapper = styled.li`
  font-family: Roboto, "Helvetica Neue", Arial;
  display: flex;
  align-items: center;
  label {
    margin-bottom: unset;
  }

  .disable-link {
    pointer-events: none;
    cursor: default;
    text-decoration: none;
  }
  a {
    color: black;
    text-decoration: none;
    &:hover {
      text-decoration: underline !important;
    }
  }
  &.disable {
    text-decoration: none;
    font-size: 14px;
    color: #000000;
    a {
      pointer-events: none;
      cursor: default;
      color: #0000008a;
      p {
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 21px;
        letter-spacing: 0.07875px;
        text-align: left;
      }
    }
  }
`

const defaultRenderBreadcrumbsList = (props) => {
  const { list, renderSeparator } = props
  return (
    list.length > 0 &&
    list.map((item, index) => {
      const { name, url = "", disable = false } = item || {}
      let linkAttr = {
        to: url.length > 0 ? url : "",
      }
      console.log(list.length - 1 + "====" + index)

      return (
        <>
          <BreadcrumWrapper className={`${disable === true ? "disable" : ""}`}>
            <Link
              className={`${url.length > 0 ? "" : "disable-link"}`}
              {...linkAttr}
            >
              {name}
            </Link>
          </BreadcrumWrapper>
          {list.length - 1 !== index && renderSeparator()}
        </>
      )
    })
  )
}

const defaultList = [
  {
    name: "Level 01",
    url: "google.com",
    disable: true,
  },
  {
    name: "Level 02",
    url: "google.com",
    disable: false,
  },
  {
    name: "Level 02",
    disable: false,
  },
]

const defaultRenderSeparator = () => (
  <BreadcrumbsSeperater>/</BreadcrumbsSeperater>
)

function Breadcrumb(props) {
  const {
    list = defaultList || [],
    renderBreadcrumbsList = defaultRenderBreadcrumbsList,
    renderSeparator = defaultRenderSeparator,
    ariaLabel = "breadcrumb",
  } = props || {}

  // separator: renderSeparator(),
  const breadcrumAttributes = {
    ariaLabel,
  }
  return (
    <BreadcrumbsKit {...breadcrumAttributes}>
      {renderBreadcrumbsList({ list, renderSeparator })}
    </BreadcrumbsKit>
  )
}

export { Breadcrumb }
export default Breadcrumb
